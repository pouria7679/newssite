<?php
    require "header.php"
?>
<?php include 'include/posts.inc.php'; ?>
<?php
        global $conn;
        $topicId = $_GET['id'];
        $sql = "SELECT * FROM articles where topic_id = $topicId";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $posts = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);

?>

<div class="main-content"> <!-- main content -->
    <div class="container">

        <div class="main-detail">
            <div class="container-fluid p-4">

                <h4>اخبار اقتصادی</h4>


                <?php foreach ($posts as $key => $post): ?>

                    <div class="row mt-3">
                        <div class="col-md-3">
                            <img src="img/<?php echo $post['image'] ?>" class="w-100 border-radius">
                        </div>
                        <div class="col-md-9">
                            <h5><?php echo $post['title'] ?></h5>
                             <p class="mt-2">
                                <?php echo substr($post['value'],0,150) . '...'?>
                             </p>
                             <p>
                                 <a href="detail.php?id=<?php echo $post['id'] ?>" class="bg-info py-1 px-3 color-fe border-radius">متن کامل خبر</a>
                             </p>

                        </div>
                    </div>

                <?php endforeach ?>


            </div>

        </div>
    </div><!-- end container -->
</div> <!-- end main content -->


<?php
    require "footer.php";
?>
