<?php 
    require "header.php"
?>
<?php 
        global $conn;
        $postId = $_GET['id'];
        $sql = "SELECT * FROM articles where id = $postId";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $post = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
       
?>
<div class="main-content"> <!-- main content -->
    <div class="container">
        
        <div class="main-detail">
            <div class="container-fluid">
                <img src="img/<?php echo $post[0]['image'] ?>" class="m-2 w-50 border-radius">
                
                <div class="detail-news">
                    <h2><?php echo $post[0]['title'] ?></h2>
                    <p>
                        <?php echo $post[0]['value']?>
                    </p>

                    <div class="bg-info d-inline-block p-3 color-fe mb-3 border-radius" >
                        <i class="fa fa-eye"></i>
                            <span class="color-fe">بازدید : <span>120</span></span>
                    </div>
                </div>
                    
            </div>

        </div>
    </div><!-- end container -->
</div> <!-- end main content -->


<?php 
    require "footer.php";
?>
        