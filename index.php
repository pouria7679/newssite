<?php
    require "header.php";
?>

    <div class="main-content">
        <div class="container">
            <div class="row"> <!-- 1st row with carousel -->
                <div class="col-lg-8">
                    <div class="owl-carousel main-carousel">
                               <?php
                                      global $conn;
                                      $sql = "SELECT * FROM articles ORDER BY id DESC LIMIT 3";
                                      $stmt = $conn->prepare($sql);
                                      $stmt->execute();
                                      $posts = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                              ?>
                               <?php foreach ($posts as $key => $post): ?>
                                                    <div class="item position-relative" style="width:861px; height:495px">
                                                            <a href="detail.php?id=<?php echo $post['id'] ?>"><img src="img/<?php echo $post['image'] ?>"></a>
                                                            <div class="carousel-caption">
                                                               <?php echo $post['title'] ?>
                                                            </div>

                                                   </div>


                               <?php endforeach ?>



                    </div>
                </div>
                <div class="col-lg-4"> <!-- pro section -->
                <?php
                   global $conn;
                   $sql = "SELECT * FROM articles where topic_id = 5  ORDER BY id DESC LIMIT 2";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                   $posts = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                ?>
                    <div class="row ">

                    <?php foreach ($posts as $key => $post): ?>
                           <div class="col-12 col-sm-6 col-lg-12 mt-2">
                                <div class="pro-news">
                                    <ahref="detail.php?id=<?php echo $post['id'] ?>">  <img src="img/<?php echo $post['image'] ?>" class="w-100 pro-img">
                                      <div class="pro-news-caption">
                                           <h5><?php echo $post['title'] ?></h5>

                                    </div></a>

                                </div>
                            </div>



                    <?php endforeach ?>

                </div> <!-- end pro section -->
            </div><!-- end 1st row with carousel -->


            <div class="title-section">
                <p>
                    <a href="#"> <h4 class="d-inline-block">ورزشی</h4> </a><i class="fa fa-caret-left"></i>
                </p>
                <div class="full-line red"></div>
            </div>
            <div class="owl-carousel section-carousel p-3"> <!-- sport section -->
                <?php
                                      global $conn;
                                      $sql = "SELECT * FROM articles where topic_id = 5 ORDER BY id DESC LIMIT 4";
                                      $stmt = $conn->prepare($sql);
                                      $stmt->execute();
                                      $economyPost = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                              ?>
                               <?php foreach ($economyPost as $key => $post): ?>

                                                  <div class="">
                                                        <div class="card-deck">
                                                                <div class="card pro-card">
                                                                       <a href="detail.php?id=<?php echo $post['id'] ?>"> <img class="card-img-top p-2 w-100" src="img/<?php echo $post['image'] ?>" alt="..">
                                                                       <div class="card-body border-top">
                                                                               <p class="card-text p-1 pb-2"><?php echo $post['title'] ?></p>
                                                                       </div></a>
                                                                       <div class="card-footer p-1 border-top-0 ">
                                                                                <div class=" card-text news-reporter d-flex align-items-center">
                                                                                      <i class="fa fa-eye"></i>
                                                                                      <small style="margin:  2px 10px;">150</small>
                                                                                </div>
                                                                       </div>
                                                                </div>
                                                        </div>
                                                  </div>

                               <?php endforeach ?>
            </div> <!-- end sport section -->

            <div class="title-section">
                <p>
                    <a href="#"> <h4 class="d-inline-block">اقتصادی</h4> </a> <i class="fa fa-caret-left"></i>
                </p>
                <div class="full-line green"></div>
            </div>
            <div class="owl-carousel section-carousel p-3"> <!-- economy section -->

               <?php
                       global $conn;
                       $sql = "SELECT * FROM articles where topic_id = 1 LIMIT 4" ;
                       $stmt = $conn->prepare($sql);
                       $stmt->execute();
                       $economyPost = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
               ?>
                <?php foreach ($economyPost as $key => $post): ?>

                                   <div class="">
                                         <div class="card-deck">
                                                 <div class="card pro-card">
                                                        <a href="detail.php?id=<?php echo $post['id'] ?>"> <img class="card-img-top p-2 w-100" src="img/<?php echo $post['image'] ?>" alt="..">
                                                        <div class="card-body border-top">
                                                                <p class="card-text p-1 pb-2"><?php echo $post['title'] ?></p>
                                                        </div></a>
                                                        <div class="card-footer p-1 border-top-0 ">
                                                                 <div class=" card-text news-reporter d-flex align-items-center">
                                                                       <i class="fa fa-eye"></i>
                                                                       <small style="margin:  2px 10px;">150</small>
                                                                 </div>
                                                        </div>
                                                 </div>
                                         </div>
                                   </div>

                <?php endforeach ?>


            </div> <!-- end economy -->

            <div class="title-section">
                <p>
                    <a href="#"> <h4 class="d-inline-block">فرهنگ و هنر</h4> </a> <i class="fa fa-caret-left"></i>
                </p>
                <div class="full-line yellow"></div>
            </div>
            <div class="owl-carousel section-carousel p-3"> <!-- cultural section -->
                 <?php
                                       global $conn;
                                       $sql = "SELECT * FROM articles where topic_id = 3 LIMIT 4";
                                       $stmt = $conn->prepare($sql);
                                       $stmt->execute();
                                       $economyPost = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                               ?>
                                <?php foreach ($economyPost as $key => $post): ?>

                                                   <div class="">
                                                         <div class="card-deck">
                                                                 <div class="card pro-card">
                                                                        <a href="detail.php?id=<?php echo $post['id'] ?>"> <img class="card-img-top p-2 w-100" src="img/<?php echo $post['image'] ?>" alt="..">
                                                                        <div class="card-body border-top">
                                                                                <p class="card-text p-1 pb-2"><?php echo $post['title'] ?></p>
                                                                        </div></a>
                                                                        <div class="card-footer p-1 border-top-0 ">
                                                                                 <div class=" card-text news-reporter d-flex align-items-center">
                                                                                       <i class="fa fa-eye"></i>
                                                                                       <small style="margin:  2px 10px;">150</small>
                                                                                 </div>
                                                                        </div>
                                                                 </div>
                                                         </div>
                                                   </div>

                                <?php endforeach ?>


            </div> <!-- end cultural -->

        </div><!-- end container -->

    </div><!-- end main content -->

   <?php
        require "footer.php";
    ?>
