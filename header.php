
<?php session_start(); ?>
<?php include 'include/db.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css " >
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css " >
    <link rel="stylesheet" href="css/style.css" type="text/css " >
    <link rel="stylesheet" href="css/style-detail.css" type="text/css " >

    <title>سایت خبری</title>

</head>
<body dir="rtl">
    <div>
        <?php
            if (isset($_SESSION['userId'])) {
                echo "شما وارد شدید";
            }
        ?>
    </div>
    <div id="my-header" class="fstHeader">
        <div class="container ">
           <div class="row">
               <div class="col-6  col-md-3 col-lg-2 settingSection">
                   <a href="index.php"> <i class="fa fa-home"></i> </a>
                   &nbsp;&nbsp;&nbsp;

                   <?php
                    if (isset($_SESSION['userId'])) {
                        echo ' <form action="include/logout.inc.php" method="post">
                            <button type="submit" name="logout-btn">خروج </button>
                        </form> ';
                    }else{
                        echo '  <button type="button"  data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">  ثبت نام </button>
                    <button type="button"  data-toggle="modal" data-target="#login" data-whatever="@mdo">ورود </button>  ';
                    }
                   ?>

               </div>

               <div class="col-12 col-md-6 col-lg-8 searchBox">

               </div>
               <div class="col-6 col-md-3 col-lg-2 logo">
                   <img src="img/logo.jpg" class="w-100">
               </div>
           </div>
        </div>
    </div>

    <!-- sign up modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">فرم ثبت نام</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="Include/signup.inc.php" method="post">
                        <div class="form-group">
                            <label for="name" class="col-form-label">نام*: </label>
                            <input type="text" class="form-control" name="name" >

                            <label for="email" class="col-form-label">ایمیل*: </label>
                            <input type="email" class="form-control" name="email" >

                            <label for="pwd" class="col-form-label"> پسورد* </label>
                            <input type="password" class="form-control" name="pwd" >

                            <input type="submit" class="btn btn-primary" name="signup-btn" value="ثبت نام">
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- end modal -->

    <!-- login modal -->
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabale" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="loginLabale">ورود</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="Include/login.inc.php" method="post">
                        <div class="form-group">

                            <label for="email" class="col-form-label">ایمیل*: </label>
                            <input type="email" class="form-control" name="emailLog" >

                            <label for="pwd" class="col-form-label"> پسورد* </label>
                            <input type="password" class="form-control" name="pwdLog" >

                            <input type="submit" class="btn btn-primary" name="login-btn" value="ورود">
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- end modal -->

    <div class="scndHeader container">
            <div class="owl-rtl owl-carousel menu-carousel">

                <?php
                    $topics = selectAll('topics');
                ?>
                <?php foreach($topics as $key=>$topic): ?>
                   <a href="category.php?id=<?php echo $topic['id'] ?>"> <?php echo $topic['name']; ?> </a>
                <?php endforeach; ?>

            </div>
            <hr>
            <div class="ad-1">

            </div>
    </div>
