<footer>
    <div class="row">
        <div class="col-lg-3">
            <h5>درباره ی ما: </h5>
            <p>
                این سایت د جهت آگاهی بیشتر افراد از آخرین خبر ها از تمام خبر گذاری های سطح دنیا می باشد که ما در جهت آن بر آمدیم تا بیشترین کمک را به شما داشته باشیم. از این جهت که مارا همراهی میکنید بسیار خوشحال هستیم.
            </p>
        </div>
        <div class="col-lg-6">
            <h5>دسترسی سریع: </h5>
            <div class="row fast-links">
                <div class="col-4">
                    <a href="#">
                        <h6><i class="fa fa-caret-left"> </i> ارتباط با ما </h6>
                    </a>
                    <a href="#">
                        <h6><i class="fa fa-caret-left"> </i> آرشیو  </h6>
                    </a>
                    <a href="#">
                        <h6><i class="fa fa-caret-left"> </i> گزارش های تصویری </h6>
                    </a>
                    <a href="#">
                        <h6><i class="fa fa-caret-left"> </i> گزارش های ویدیوییا </h6>
                    </a>
                </div>
                <div class="col-4">
                    <a href="#">
                        <h6><i class="fa fa-caret-left"> </i> اقتصادیا </h6>
                    </a>
                    <a href="#">
                        <h6> <i class="fa fa-caret-left"> </i> ورزشی  </h6>
                    </a>
                    <a href="#">
                        <h6> <i class="fa fa-caret-left"> </i> فرهنگی هنری </h6>
                    </a>
                    <a href="#">
                        <h6><i class="fa fa-caret-left"> </i>  سبک زندگی </h6>
                    </a>
                </div>
                <div class="col-4">
                    <a href="#">
                        <h6><i class="fa fa-caret-left"> </i> اقتصادیا </h6>
                    </a>
                    <a href="#">
                        <h6> <i class="fa fa-caret-left"> </i> ورزشی  </h6>
                    </a>
                    <a href="#">
                        <h6> <i class="fa fa-caret-left"> </i> فرهنگی هنری </h6>
                    </a>
                    <a href="#">
                        <h6><i class="fa fa-caret-left"> </i>  سبک زندگی </h6>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <h5> راه های ارتباطی با ما:</h5>
            <div class="contact-us scale11">
                <a href="#"> <img src="img/insta.png"></a>
                <a href="#"> <img src="img/telegram.png"></a>
                <a href="#"> <img src="img/aparat.png"></a>
            </div>
            <div class="apps scale11">
                <a href="#"> <img src="img/bazar.png"></a>
                <a href="#">  <img src="img/google-play.png"></a>
            </div>
        </div>
    </div>
    <div class="copyrights">
        &copy; تمام حقوق مادی و معنوی این ایت متعلق به شرکت برنامه نویسی سخت جویان شمال و شمال شرق می باشد
    </div>
</footer>


<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script> -->
<script src="js/jquery.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<script>

    $(document).ready(function(){
        $(".menu-carousel").owlCarousel({
            autoWidth: true,
            margin:15,
            rtl: true,
            nav: true

        });
        $(".main-carousel").owlCarousel({
            autoWidth: true,
            rtl: true,
            nav: true,
            dots: true,
            autoplay:true,
            items:1,
            autoplaySpeed: 2000,
            autoplayTimeout: 9000,

        });
        $(".section-carousel").owlCarousel({
            rtl: true,
            nav: true,
            items:4,
            margin:30,
            autoplaySpeed: 2000,
            autoplayTimeout: 9000,
            responsiveClass: true,
            responsive: {
                1350:{
                    items:4
                },
                1200:{
                    items:3
                },
                768:{
                    items:2
                },
                0:{
                    items:1
                }
            }

        });
    });

</script>
</body>
</html>