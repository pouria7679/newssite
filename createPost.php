
<?php require "header.php"; ?>
<?php include 'include/posts.inc.php'; ?>

<div class="main-content"> <!-- main content -->
    <div class="container">
        
        <div class="main-detail">
            <div class="container-fluid p-4">
            	<h4>ثبت خبر جدید </h4>
            	<hr>
                
               <form action="createPost.php" method="post" enctype='multipart/form-data'>
               	
               	<div>
               		<label>موضوع: </label>
	               	<input class="form-control" type="text" name="title">
               	</div>

               	<div class="mt-2">
					<label>متن: </label>
               		<textarea class="form-control" name="body"></textarea>
               	</div>
               	
               	<div class="mt-2">
               		<label>دسته بندی:</label>
               		<select name="topic_id">

               			<?php foreach ($topics as $key => $topic): ?>
               				<option class="p-2" value="<?php echo $topic['id'] ?>"><?php echo $topic['name'] ?></option>
               			<?php endforeach ?> 
					
					</select>
               	</div>

               	<div class="mt-2">
					<label>تصویر:</label>
               		<input class="form-control border-0" type="file" name="image">
               	</div>

               	<input class="btn btn-info mt-3" value="ثبت " type="submit" name="create-post">

               </form>               
                    
            </div>

        </div>
    </div><!-- end container -->
</div> <!-- end main content -->


<?php 
    require "footer.php";
?>
        