<?php
	

	require('dbh.inc.php');

	function dd($value){
		echo "<pre>", print_r($value, true), "</pre>";
		die();
	}

	function selectAll($table){
		global $conn;
		$sql = "SELECT * FROM $table";
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		$records = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
		return $records;
	}

	/*function create($table, $data){

		global $conn;
		$sql = "INSERT INTO $table SET ";
		$i = 0;
		foreach ($data as $key => $value) {
			if ($i === 0) {
				$sql = $sql . " $key=?";
			}else{
				$sql = $sql . ", $key=?";
			}
			$i++;
		}
		$stmt = executeQuery($sql, $data);
		$id = $stmt->insert_id;
		return $id;
	}*/


