<?php 

	if (isset($_POST['signup-btn'])) {
		require "dbh.inc.php";


		$userName = $_POST['name'];
		$userEmail = $_POST['email'];
		$userpwd = $_POST['pwd'];

		if (empty($userName) || empty($userEmail) || empty($userpwd) ) {
			header("Location: ../index.php?emptyField");
			exit();
		}
		 
		else{
			$sql = "SELECT email FROM users WHERE email = ?";
			$stmt = mysqli_stmt_init($conn);
			if (!mysqli_stmt_prepare($stmt , $sql)) {
				header("Location: ../index.php?sqlError");
			exit();
			}
			else{
				mysqli_stmt_bind_param($stmt, "s", $userEmail);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_store_result($stmt);
				$resultCheck = mysqli_stmt_num_rows($stmt);
				if ($resultCheck > 0) {
					header("Location: ../index.php?error=emailTaken");
					exit();
				}else{

					$sql = "INSERT INTO users(name, email, password) VALUES (?,?,?)";
					$stmt = mysqli_stmt_init($conn);
					if (!mysqli_stmt_prepare($stmt , $sql)) {
						header("Location: ../index.php?sqlError");
						exit();
					}else{
						$hashPwd = password_hash($userpwd, PASSWORD_DEFAULT);
						mysqli_stmt_bind_param($stmt, "sss", $userName, $userEmail, $hashPwd);
						mysqli_stmt_execute($stmt);
						header("Location: ../index.php?signup=success");
						exit();
					}
				}

			}
		}

		mysqli_stmt_close($stmt);
		mysqli_stmt_close($conn);

	}else{

		header("Location: ../index.php?");
		exit();

	}