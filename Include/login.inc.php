<?php

	if (isset($_POST['login-btn'])) {
		require 'dbh.inc.php';

		$email = $_POST['emailLog'];
		$pwd = $_POST['pwdLog'];

		if (empty($email) || empty($pwd)) {
			header("Location: ../index.php?error=emptyfields");
			exit();
		}else{

			 $sql = "SELECT * FROM users WHERE email=? ";
			 $stmt = mysqli_stmt_init($conn);
			 if (!mysqli_stmt_prepare($stmt, $sql)) {
			 	header("Location: ../index.php?error=sqlError");
			exit();
			 }else{
			 	mysqli_stmt_bind_param($stmt, "s", $email);
				mysqli_stmt_execute($stmt);
				$result = mysqli_stmt_get_result($stmt);
				if ($row = mysqli_fetch_assoc($result)) {
					$pwdCheck = password_verify($pwd, $row['password']);
					if ($pwdCheck == false) {
						header("Location: ../index.php?error=wrongPassword");
						exit();
					}else if ($pwdCheck == true) {
						session_start();
						$_SESSION[userId] = $row[id];
						$_SESSION[userName] = $row[name];
						header("Location: ../index.php?login=success");
						exit(); 
					}else{
						header("Location: ../index.php?error=wrongPassword");
						exit();
					}
				}else{
					header("Location: ../index.php?error=noUser");
			exit();
				}
			 }

		}

	}else{
		header("Location: ../index.php");
		exit();
	}